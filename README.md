# Código Único de Factura - CUF

Permite generar el Código Único de Factura - CUF para las facturas del Servicio
de Impuestos Nacionales - SIN.

## Instalación

Clonar el repositorio e instalar la gema:

```bash
gem build codigo-unico-factura.gemspec
gem install ./codigo-unico-factura-0.1.0.gem
```

## Modo de uso

```bash
require 'codigo-unico-factura'

codigo = CodigoUnicoFactura.generar(
          '123456789', # NIT del emisor
          '20190113163721231', # Fecha y hora
          '0', # Sucursal
          '1', # Modalidad
          '1', # Tipo de emisión
          '1', # Código documento fiscal
          '1', # Tipo documento sector
          '1', # Número de factura
          '0', # Punto de venta
         )

=> "159FFE6FB1986A24BB32DBE5A2A34214B245A6A3"
```

## Test

```bash
rake test
```

```bash
Run options: --seed 10715
# Running:
.
Finished in 0.004917s, 203.3830 runs/s, 10169.1516 assertions/s.
1 runs, 50 assertions, 0 failures, 0 errors, 0 skips
```

## Bibliografía

* http://siatinfo.impuestos.gob.bo/index.php/certificacion-de-sistemas/inicio-de-certificacion-4/etapa-generacion-cuf
* https://guides.rubygems.org/make-your-own-gem/
