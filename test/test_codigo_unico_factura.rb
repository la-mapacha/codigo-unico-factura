require 'csv'
require 'minitest/autorun'
require 'codigo-unico-factura'

class CodigoUnicoFacturaTest < Minitest::Test
  def test_codigo_unico_factura
    archivo = '/test/archivos/casos-de-prueba-cuf.csv'
    nombre_archivo = File.dirname(File.dirname(File.expand_path(__FILE__))) + archivo
    CSV.foreach(nombre_archivo, col_sep: ',', headers: true) do |fila|
      assert_equal fila['APLICAR BASE 16'],
       CodigoUnicoFactura.generar(
          fila['NIT EMISOR'], # NIT del emisor
          fila['FECHA/HORA'], # Fecha y hora
          fila['SUCURSAL'], # Sucursal
          fila['MODALIDAD'], # Modalidad
          fila['TIPOEMISION'], # Tipo de emisión
          fila['CODIGODOCUMENTOFISCAL'], # Código documento fiscal
          fila['TIPODOCUMENTOSECTOR'], # Tipo documento sector
          fila['NUMERO DE FACTURA'], # Número de factura
          fila['POS'], # Punto de venta
        )
    end
  end
end
