Gem::Specification.new do |s|
  s.name        = 'codigo-unico-factura'
  s.version     = '0.1.0'
  s.date        = '2019-06-06'
  s.summary     = "Generador del Código Único de Factura"
  s.description = "Generador del Código Único de Factura"
  s.authors     = ["Elmer Mendoza"]
  s.email       = 'defreddyelmer@gmail.com'
  s.files       = Dir.glob("{lib}/**/**/*") + ["Rakefile"]
  s.homepage    = 'https://gitlab.com/la-mapacha/codigo-unico-factura'
  s.license     = 'MIT'
end
