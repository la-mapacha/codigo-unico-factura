# coding: utf-8

class CodigoUnicoFactura
  def self.generar(
    nit, 
    fecha_hora, 
    sucursal, 
    modalidad, 
    tipo_emision, 
    codigo_documento_fiscal, 
    tipo_documento_sector, 
    nro_factura, 
    pos
  )
    # 1. Completar los campos según la longitud definida con ceros a la izquierda
    nit = nit.to_s.rjust(13, '0')
    fecha_hora = fecha_hora.to_s.rjust(17, '0')
    sucursal = sucursal.to_s.rjust(4, '0')
    modalidad = modalidad.to_s.rjust(1, '0')
    tipo_emision = tipo_emision.to_s.rjust(1, '0')
    codigo_documento_fiscal = codigo_documento_fiscal.to_s.rjust(1, '0')
    tipo_documento_sector = tipo_documento_sector.to_s.rjust(2, '0')
    nro_factura = nro_factura.to_s.rjust(8, '0')
    pos = pos.to_s.rjust(4, '0')

    # 2. Se concatena los campos
    cadena = [
      nit, 
      fecha_hora, 
      sucursal, 
      modalidad, 
      tipo_emision, 
      codigo_documento_fiscal, 
      tipo_documento_sector, 
      nro_factura, 
      pos
    ].join

    # 3. Módulo 11 de la cadena
    codigo_autoverificador = modulo_11(cadena, 1, 9, false)
    cadena = [cadena, codigo_autoverificador].join

    # 4. Aplicar Base 16
    cadena.to_i.to_s(16).upcase
  end

  private

  # Algoritmo extractado desde http://siatinfo.impuestos.gob.bo/index.php/menu-uno-12/algoritmo-modulo-11
  def self.modulo_11(cadena, num_dig, lim_mult, x10)
    num_dig = 1 if !x10
    (1..num_dig).each do |n|
      soma = 0
      mult = 2
      (cadena.length - 1).downto(0) do |i|
        soma += (mult * cadena[i].to_i)
        mult += 1
        mult = 2 if mult > lim_mult
      end
      dig = x10 ? ((soma * 10) % 11) % 10 : soma % 11
      cadena += '1' if dig == 10
      cadena += '0' if dig == 11
      cadena += dig.to_s if dig < 10
    end
    cadena[cadena.length - num_dig, cadena.length]
  end
end
